package ru.t1.sukhorukova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-logout";
    @NotNull private final String DESCRIPTION = "Log out.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
