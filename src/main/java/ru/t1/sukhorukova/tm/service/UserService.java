package ru.t1.sukhorukova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.UserNotFoundException;
import ru.t1.sukhorukova.tm.exception.field.EmailEmptyException;
import ru.t1.sukhorukova.tm.exception.field.IdEmptyException;
import ru.t1.sukhorukova.tm.exception.field.LoginEmptyException;
import ru.t1.sukhorukova.tm.exception.field.PasswordEmptyException;
import ru.t1.sukhorukova.tm.exception.user.ExistsEmailException;
import ru.t1.sukhorukova.tm.exception.user.RoleEmptyException;
import ru.t1.sukhorukova.tm.model.User;
import ru.t1.sukhorukova.tm.util.HashUtil;
import ru.t1.sukhorukova.tm.api.service.IUserService;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull private final IProjectRepository projectRepository;
    @NotNull private final ITaskRepository taskRepository;
    @NotNull private final IPropertyService propertyService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserRepository userRepository,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        super(userRepository);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw  new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return add(user);
    }

    @NotNull @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw  new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();

        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw  new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final User user = create(login, password);
        return user;
    }

    @Nullable @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @Nullable final User user = repository.findByLogin(login);
        if (user == null) return null;
        return user;
    }

    @Nullable @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @Nullable final User user = repository.findByEmail(email);
        if (user == null) return null;
        return user;
    }

    @Nullable @Override
    public User removeOne(@Nullable final User model) {
        if (model == null) return null;

        @Nullable final User user = super.removeOne(model);
        if (user == null) return null;
        @NotNull final String userId = user.getId();
        taskRepository.removeAll(userId);
        projectRepository.removeAll(userId);
        return user;
    }

    @Nullable @Override
    public User removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @Nullable @Override
    public User removeOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @Nullable final User user = findByEmail(email);
        if (user == null) throw new EntityNotFoundException();
        return repository.removeOne(user);
    }

    @NotNull @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final User user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final User user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    private User setLockOneByLogin(
            @Nullable final String login,
            @NotNull final Boolean locked
    ) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(locked);
        return user;
    }

    @NotNull @Override
    public User lockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, true);
    }

    @NotNull @Override
    public User unlockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, false);
    }

    @NotNull @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @NotNull @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

}
