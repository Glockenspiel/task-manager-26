package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull public static final String NAME = "task-create";
    @NotNull public static final String DESCRIPTION = "Create new task.";

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");

        System.out.println("Enter task name:");
        @Nullable final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description");
        @Nullable final String description = TerminalUtil.nextLine();

        getTaskService().create(getAuthService().getUserId(), name, description);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
