package ru.t1.sukhorukova.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Setter @Getter
public abstract class AbstractModel {

    @NotNull private String id = UUID.randomUUID().toString();

}
